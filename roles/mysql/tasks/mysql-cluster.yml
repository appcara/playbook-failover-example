---
- name: update apt cache
  apt: update_cache=yes

- name: install MySQLdb python module
  package: name=python-mysqldb state=present
  register: python_mysqldb_installed

- name: install apt python module
  package: name=python-apt state=present

- name: add mariadb key file
  apt_key: keyserver='hkp://keyserver.ubuntu.com:80' id=0xcbcb082a1bb943db state=present

- name: add mariadb repo
  apt_repository: repo="deb [arch=amd64,i386] {{ mariadb_repo }} {{ ansible_distribution_release }} main" state=present

- name: install debconf
  package: name={{ item }} state=present
  with_items:
    - debconf
    - debconf-utils
  register: debconf_installed

- name: set MySQL root password
  when: debconf_installed|success
  debconf: name="mariadb-server-{{ mariadb_version }}" question={{ item }} value={{ mysql_root_password }} vtype=password
  with_items:
    - 'mysql-server/root_password'
    - 'mysql-server/root_password_again'

- name: install MySQL server
  package: name=mariadb-server state=present
  register: mysql_installed

- name: copy .my.cnf file for root
  when: (mysql_installed|success) and (python_mysqldb_installed|success)
  template: src=.my.cnf.j2 dest=/root/.my.cnf owner=root mode=600

- name: start mysql service
  when: mysql_installed|success
  service: name=mysql state=started enabled=no

- name: remove anonymous accounts
  when: (mysql_installed|changed) and (python_mysqldb_installed|success)
  mysql_user: name='' host={{ item }} state=absent
  with_items:
    - "localhost"
    - "127.0.0.1"
    - "{{ ansible_hostname }}"
    - "%"
    - "::1"

- name: add mysql config
  template: src=my.cnf.j2 dest=/etc/mysql/my.cnf owner=root group=root
  register: mysql_config

- name: add cluster config
  template: src=cluster.cnf.j2 dest=/etc/mysql/conf.d/cluster.cnf owner=root group=root
  register: cluster_config

- name: unconfigure cluster
  when: cluster_config|changed
  file: path='/etc/mysql/galera_cluster_configured' state=absent

- name: check if cluster is setup
  stat: path='/etc/mysql/galera_cluster_configured'
  register: galera_cluster_configured

- name: stop mysql service
  when: not galera_cluster_configured.stat.exists
  service: name=mysql state=stopped enabled=no

- name: ensure all mysql process is stopped
  when: not galera_cluster_configured.stat.exists
  command: pkill -9 mysqld
  ignore_errors: true

- name: bootstrapping first node
  when: (not galera_cluster_configured.stat.exists) and (inventory_hostname == groups['mysql'][0])
  command: nohup service mysql bootstrap

- name: forming the cluster
  when: (not galera_cluster_configured.stat.exists) and (inventory_hostname != groups['mysql'][0])
  service: name=mysql state=restarted

- name: mark cluster as configured
  when: not galera_cluster_configured.stat.exists
  file: path='/etc/mysql/galera_cluster_configured' state=touch

- name: fetch the debian mysql config
  when: inventory_hostname == groups['mysql'][0]
  fetch: src='/etc/mysql/debian.cnf' dest='./debian_mysql_user'
  tags:
    - sync_debian_user

- name: transfer debian mysql to other nodes
  when: inventory_hostname != groups['mysql'][0]
  copy: src='./debian_mysql_user/{{groups['mysql'][0]}}/etc/mysql/debian.cnf' dest='/etc/mysql/debian.cnf'
  tags:
    - sync_debian_user

- name: clean up temp files
  local_action:
    module: file
    path: './debian_mysql_user'
    state: absent
  run_once: true
  tags:
    - sync_debian_user
